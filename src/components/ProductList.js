import * as React from "react";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";

export default function ProductList({ products, columns }) {
  products.map((p) => {
    if (p.subscription === true) {
      p.subscription = "Yes";
    } else if (p.subscription === false) {
      p.subscription = "No";
    }
    return p;
  });

  return (
    <div style={{ display: "flex", height: "500px" }}>
      <div style={{ flexGrow: 1 }}>
        {products.length > 0 && (
          <>
            <DataGrid
              rows={products}
              columns={columns}
              pageSize={5}
              rowsPerPageOptions={[5]}
              checkboxSelection
              disableSelectionOnClick
              components={{ Toolbar: GridToolbar }}
            />
          </>
        )}
      </div>
    </div>
  );
}
