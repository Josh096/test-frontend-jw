import { useState, useEffect } from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import ProductList from "./components/ProductList";
import Loading from "./components/Loading";
import axios from "axios";
import getColumnConfig from "./helpers/column-helper";

function App() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const columns = getColumnConfig();

  useEffect(() => {
    axios.get(process.env.REACT_APP_JSON_SERVER).then((res) => {
      setLoading(false);
      setData(res.data);
    });
  }, []);

  return (
    <div>
      <Navbar />
      {loading ? (
        <Loading />
      ) : (
        <ProductList products={data} columns={columns} />
      )}
    </div>
  );
}

export default App;
