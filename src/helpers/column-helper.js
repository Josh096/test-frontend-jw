import CheckCircle from "@mui/icons-material/CheckCircle";
import Cancel from "@mui/icons-material/Cancel";
import Avatar from "@mui/material/Avatar";
import Chip from "@mui/material/Chip";
import Link from "@mui/material/Link";

export default function getColumnConfig() {
  return [
    { field: "id", headerName: "ID", width: 90 },
    {
      field: "title",
      headerName: "Product",
      width: 200,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => (
        <Link href={params.row.url} color="inherit">
          {params.row.title}
        </Link>
      ),
    },
    {
      field: "vendor",
      headerName: "Vendor",
      width: 150,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "price",
      headerName: "Price",
      type: "number",
      width: 110,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => `£${params.row.price}`,
    },
    {
      field: "tags",
      headerName: "Tags",
      width: 250,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => {
        return params.row.tags.map((t) => {
          return <Chip key={t} style={{ margin: "5px" }} label={t} />;
        });
      },
    },
    {
      field: "subscription",
      headerName: "Subscription",
      width: 150,
      renderCell: (params) => {
        if (params.row.subscription === "Yes") {
          return (
            <>
              <CheckCircle style={{ color: "#27ae60", marginRight: "5px" }} />
              {params.row.subscription_discount}%
            </>
          );
        } else if (params.row.subscription === "No") {
          return <Cancel style={{ color: "#e74c3c" }} />;
        }
      },
      headerAlign: "center",
      align: "center",
    },
    {
      field: "image_src",
      headerName: "Product Image",
      width: 150,
      renderCell: (params) => {
        return <Avatar src={params.row.image_src} alt={params.row.title} />;
      },
      headerAlign: "center",
      align: "center",
    },
    {
      field: "url",
      headerName: "",
      width: 200,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => (
        <Link href={params.row.url} color="inherit">
          Buy
        </Link>
      ),
    },
  ];
}
