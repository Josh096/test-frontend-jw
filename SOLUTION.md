SOLUTION
========

Estimation
----------
Estimated: 2 hours

Spent: 2 hours


Solution
--------

Thank you for taking the time to view and test my solution. As a foreword I would like to comment specifically on the final product filtering scenario. During the design stage I chose to go with Material UI's DataGrid component which includes several useful features like filtering, searching and pagination. Upon completion of the solution I found that the multi filter feature was removed from the free version of Material UI which I have previously used. I think my solution is still fit for purpose and demonstrates my understanding of the requirements featuring a robust filtering mechanism with more features than mentioned, just without the minor functionality of multi filtering which can be included with Material UI Pro. If I were to redo test using another method, I would use another library I am familiar with which would include writing more code (something I generally disagree with): https://www.npmjs.com/package/react-filter-search

I amended the last but one product ID to increment with the collection. Instead of writing some code to handle the duplicate IDs, I decided to change the ID as the code test allows me to modify any file in the repository. I would like to hope that something like an ID being stored in data is unique and we can use this as a unique identifier for each product. Alternatively we could differentiate by something like the SKU or slug.

Improvements
----

Given more time I would give some considerations to the following:

- A more mobile-friendly display of the data when viewing on mobile. (Table is responsive)
- Obtaining correct product images for each product for a better user experience when browsing. (Image-centric design)
- Product page for each product, including descriptions and further details.
- Unit tests
(And the inclusion of multi-filtering, as mentioned previously)

Testing scenarios
---

``` gherkin
GIVEN I visit the product page
THEN I expect to see filters sidebar
AND I expect to see a table of products
AND I expect to see products "1-5" of 12 in a table
AND I expect to see a next page arrow button

GIVEN I visit the product page
THEN I expect to see filters sidebar for tags
WHEN I search for "Subscription" contains "yes" in filters sidebar
THEN I expect to see 7 products in the resulting table

GIVEN I visit the product page
THEN I expect to see filters sidebar
WHEN I filter by "Product" contains "formula" in the sidebar
THEN I expect to see 2 products in the resulting table

GIVEN I visit the product page
THEN I expect to see filters sidebar
WHEN I filter by "Price" > "50" in the sidebar
THEN I expect to see 3 products in the resulting table

GIVEN I visit the product page
THEN I expect to see filters sidebar
WHEN I filter by "Subscription" contains "no" in the sidebar
THEN I expect to see 5 products in the resulting table

```

Edge Cases
---

- Data does not load from local json-server, loading icon is shown.
- Images are not loaded from source, alt text is shown.
- Data source is updated, more data is loaded than the browser can handle and we must paginate via the API.